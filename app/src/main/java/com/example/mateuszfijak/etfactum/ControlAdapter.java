package com.example.mateuszfijak.etfactum;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class ControlAdapter
{
    //current panel context
    Context panelContext;

    //data fot bluetooth connection
    BluetoothAdapter bluetoothAdapter = null;
    BluetoothSocket bluetoothSocket = null;
    String adapterAddress = null;
    String adapterName = null;
    Set<BluetoothDevice> pairedDevice;
    static final UUID controlAdapterUUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");


    public ControlAdapter(Context context)
    {
        panelContext = context;
    }


    public void connectDevice() throws Exception
    {
        try
        {
            //accessing your phone bluetooth
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            adapterAddress = bluetoothAdapter.getAddress();

            //accessing paired device
            pairedDevice = bluetoothAdapter.getBondedDevices();

            if(pairedDevice.size() > 0)
            {
                for(BluetoothDevice bluetoothDevice: pairedDevice)
                {
                    adapterAddress = bluetoothDevice.getAddress();
                    adapterName = bluetoothDevice.getName().toString();
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            //Initializing bluetooth objects
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(adapterAddress);
            bluetoothSocket = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(controlAdapterUUID);

        }
        bluetoothSocket.connect();

        //making alert dialog with user
        AlertDialog.Builder builder = new AlertDialog.Builder(panelContext);
        builder.setMessage("Detected device: " + adapterName + "\n : " + adapterAddress);
        builder.setCancelable(true);
        builder.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try
                {
                    bluetoothSocket.connect();
                    Toast.makeText(panelContext, "Connected", Toast.LENGTH_LONG).show();
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                finally
                {
                    dialog.cancel();
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        //displaying alert dialog
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.setTitle("Device detected!");
        alertDialog.show();

    }


    public void sendMessage(char message)
    {
        try
        {
            bluetoothSocket.getOutputStream().write(message);
        }
        catch (Exception ex)
        {
            Toast.makeText(panelContext, "connection error", Toast.LENGTH_SHORT);
            ex.printStackTrace();
        }
    }


    public String getMessage()
    {
        String inMessage = null;

        try
        {
            inMessage = String.valueOf(bluetoothSocket.getInputStream());
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }

        return inMessage;
    }

}
