package com.example.mateuszfijak.etfactum;

import android.graphics.Canvas;
import android.view.SurfaceHolder;


public class DrawThread extends Thread
{
    //fps limit
    private final int FPS = 100;

    //common drawing stuff
    private SurfaceHolder surfaceHolder;
    public static Canvas canvas;

    private ControlEngine controlEngine;

    //running state
    private boolean running;


    public DrawThread(SurfaceHolder surfaceHolder, ControlEngine controlEngine) {
        super();
        this.surfaceHolder = surfaceHolder;
        this.controlEngine = controlEngine;
    }


    @Override
    public void run() {
        //initializing fsp limiting and counters
        long startTime;
        long endTime;
        long waitTime;
        long targetTime = 1000 / FPS;

        while (running) {
            //starting timer
            startTime = System.nanoTime();

            try {
                //locking canvas
                canvas = this.surfaceHolder.lockCanvas();

                this.controlEngine.draw(canvas);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (canvas != null) {
                    try {
                        //unlocking canvas
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            //calculating this loop time
            endTime = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - endTime;

            //if the loop was shorter then target sleep to keep static fps
            try {
                this.sleep(waitTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setRunning(boolean b) {
        running = b;
    }

}
