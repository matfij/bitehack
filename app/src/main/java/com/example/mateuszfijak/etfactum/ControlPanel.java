package com.example.mateuszfijak.etfactum;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;


public class ControlPanel extends Activity
{
    private ControlEngine controlEngine;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //set to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //getting screen resolution
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        //starting the game
        controlEngine = new ControlEngine(this, size.x, size.y);
        setContentView(controlEngine);
    }


    @Override
    protected void onPause()
    {
        super.onPause();

        if(!controlEngine.isGameEnded())
        {
            //going back to menu
            Intent intent = new Intent(getApplicationContext(), MainMenu.class);
            startActivity(intent);

            this.finish();
        }
    }


    @Override
    protected void onResume()
    {
        //resuming game
        controlEngine.resume();
        super.onResume();
    }


    @Override
    public void onBackPressed()
    {
        if(controlEngine.isPaused())
        {
            //going back to menu
            Intent intent = new Intent(getApplicationContext(), MainMenu.class);
            startActivity(intent);
        }
        controlEngine.pause();
    }
}
