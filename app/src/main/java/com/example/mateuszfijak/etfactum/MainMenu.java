package com.example.mateuszfijak.etfactum;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;


public class MainMenu extends Activity
{
    //bluetooth adapter
    static ControlAdapter controlAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //making activity fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }


    @Override
    public void onBackPressed()
    {
        //quiting the app
        //android.os.Process.killProcess(android.os.Process.myPid());
        //System.exit(0);
    }


    //button listeners
    public void menuClick(View view)
    {
        //depending on which button is clicked start new activity
        switch (view.getId())
        {

            case R.id.challengeButton:
            {
                //starting challenge activity
                Intent intent = new Intent(getApplicationContext(), ControlPanel.class);
                startActivity(intent);

                break;
            }


            case R.id.settingsButton:
            {
                //connecting bluetooth
                //initializing control adapter as bluetooth adapter
                controlAdapter = new ControlAdapter(this);
                try {
                    controlAdapter.connectDevice();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
