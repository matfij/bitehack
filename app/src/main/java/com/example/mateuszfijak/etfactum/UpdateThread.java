package com.example.mateuszfijak.etfactum;




public class UpdateThread extends Thread
{
    //fps limit
    private final int FPS = 100;

    private ControlEngine controlEngine;

    //running state
    private boolean running;


    public UpdateThread(ControlEngine controlEngine)
    {
        super();
        this.controlEngine = controlEngine;
    }



    @Override
    public void run()
    {
        //initializing fsp limiting and counters
        long startTime;
        long endTime;
        long waitTime;
        long targetTime = 1000/FPS;

        while(running)
        {
            //starting timer
            startTime = System.nanoTime();

            //updating canvas
            this.controlEngine.update();


            //calculating this loop time
            endTime = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - endTime;

            //if the loop was shorter then target sleep to keep static fps
            try
            {
                this.sleep(waitTime);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void setRunning(boolean b) { running = b; }

    public int getFPS() { return FPS; }

}

