package com.example.mateuszfijak.etfactum;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;



public class ControlEngine extends SurfaceView implements SurfaceHolder.Callback
{

    //executive threads
    private DrawThread drawThread;
    private UpdateThread updateThread;
    private int fps;

    //control status
    private boolean gameEnded;
    private boolean isPaused;

    //screen size for proper scaling
    private int screenX;
    private int screenY;

    //solids
    private Point point, post;
    private Rocket rocket, button;

    //background
    private Background background;

    //application context
    private Context context;

    //pointer cords
    int x = 0,y = 0;
    char xMes = '0', yMes = '0';


    public ControlEngine(Context context, int screenX, int screenY)
    {
        super(context);

        //adding the callback to the surface holder to intercept events
        getHolder().addCallback(this);

        //making gamePanel focusable so it can handle events
        setFocusable(true);

        //initializing screen size fo proper scale
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing game threads
        drawThread = new DrawThread(getHolder(), this);
        updateThread = new UpdateThread(this);
        fps = updateThread.getFPS();

        //initializing shape
        rocket = new Rocket(screenX, screenY, Color.rgb(192,192,192), Color.rgb(211,211,211));
        point = new Point(screenX/2,(int)(0.4*screenY));

        button = new Rocket(screenX, screenY, Color.rgb(220,220,220), Color.rgb(112,127,144));
        post = new Point(screenX/2,(int)(0.9*screenY));

        //initializing background
        background = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.back), BitmapFactory.decodeResource(getResources(), R.drawable.stardust), screenY);

        //initializing context - allows to start new activity from a SurfaceView
        this.context = context;

        //resuming game
        isPaused = false;
        gameEnded = false;

    }


    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        //starting the game loop
        drawThread.setRunning(true);
        drawThread.start();
        updateThread.setRunning(true);
        updateThread.start();
    }


    public void update()
    {
        //updating background
        background.update(fps);

        //updating positions of active objects;
        rocket.update(point);
        button.update(post);

    }

    public void draw(Canvas canvas)
    {
        super.draw(canvas);

        if(!isPaused)
        {
            //initializing paint
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);

            //drawing objects
            background.draw(canvas);
            rocket.draw(canvas);

            button.draw(canvas);
            paint.setTextSize(screenX/7);
            paint.setColor(Color.BLACK);
            canvas.drawText("X", (float)(0.455*screenX), (float) (0.93 * screenY), paint);

        }

        if(isPaused)
        {
            //initializing paint
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);

            //drawing objects
            background.draw(canvas);
            rocket.draw(canvas);

            button.draw(canvas);
            paint.setTextSize(screenX/7);
            paint.setColor(Color.BLACK);
            canvas.drawText("X", (float)(0.455*screenX), (float) (0.93 * screenY), paint);

            //shadow box
            canvas.drawColor(Color.argb(200, 64, 64, 64));
        }
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}


    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        boolean retry = true;
        while(retry)
        {
            try
            {
                drawThread.setRunning(false);
                drawThread.join();
                updateThread.setRunning(false);
                updateThread.join();
                background.clear();
            }
            catch(InterruptedException e)
            {
                e.printStackTrace();
            }
            retry = false;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event)
    {

        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
            {
                resume();

                x = (int)event.getX();
                y = (int)event.getY();

                if(y > 0.75*screenY && (x > 0.3*screenX && x < 0.75*screenX))
                {
                    MainMenu.controlAdapter.sendMessage('x');
                    Log.e("btc", "button");
                }

                break;
            }

            case MotionEvent.ACTION_MOVE:
            {
                x = (int)event.getX();
                y = (int)event.getY();

                if(y < 0.75*screenY)
                {
                    point.set((int)event.getX(), (int)event.getY());

                    //calculating x cord
                    if(x < 0.15*screenX)                      xMes = '1';    // left
                    else if(0.15*screenX <= x && x < 0.2*screenX)  xMes = '2';
                    else if(0.2*screenX <= x && x < 0.3*screenX)   xMes = '3';
                    else if(0.3*screenX <= x && x < 0.4*screenX)   xMes = '4';
                    else if(0.4*screenX <= x && x < 0.55*screenX)   xMes = '5';    // center
                    else if(0.55*screenX <= x && x < 0.6*screenX)   xMes = '6';
                    else if(0.6*screenX <= x && x < 0.7*screenX)   xMes = '7';
                    else if(0.7*screenX <= x && x < 0.85*screenX)  xMes = '8';
                    else                                           xMes = '9';   // right

                    //calculating y cord
                    if(y < 0.08*screenY)                      yMes = '9';    // left
                    else if(0.08*screenY <= y && y < 0.16*screenY)  yMes = '8';
                    else if(0.16*screenY <= y && y < 0.24*screenY)   yMes = '7';
                    else if(0.24*screenY <= y && y < 0.32*screenY)   yMes = '6';
                    else if(0.32*screenY <= y && y < 0.45*screenY)   yMes = '5';    // center
                    else if(0.45*screenY <= y && y < 0.53*screenY)   yMes = '4';
                    else if(0.53*screenY <= y && y < 0.6*screenY)   yMes = '3';
                    else if(0.6*screenY <= y && y < 0.75*screenY)  yMes = '2';
                    else                                           yMes = '1';   // right


                    MainMenu.controlAdapter.sendMessage('a');
                    MainMenu.controlAdapter.sendMessage(xMes);
                    MainMenu.controlAdapter.sendMessage(yMes);
                }

                break;
            }
            case MotionEvent.ACTION_UP:
            {
                //sending message on button click
                point.set(screenX/2,(int)(0.4*screenY));
                MainMenu.controlAdapter.sendMessage('a');
                MainMenu.controlAdapter.sendMessage('5');
                MainMenu.controlAdapter.sendMessage('5');
                Log.e("btc", "wracam na srodek");
                break;
            }
        }
        return true;
    }


    public void pause()
    {
        if(!isPaused)
        {
            isPaused = true;
        }
    }


    public void resume()
    {
        if(isPaused)
        {
            isPaused = false;
        }
    }


    public boolean isPaused() { return isPaused; }

    public boolean isGameEnded() { return gameEnded; }
}