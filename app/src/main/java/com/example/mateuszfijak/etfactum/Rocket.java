package com.example.mateuszfijak.etfactum;


import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;


public class Rocket
{

    //texture
    private RectF shape;
    private float sizeX;
    private float sizeY;
    private int color;
    private int borderColor;



    //screen size for proper size scalling
    private int screenX;
    private int screenY;


    public Rocket(int screenX, int screenY, int color, int borderColor)
    {

        //initializing screen size
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing player's body
        shape = new RectF();
        this.color = color;
        this.borderColor = borderColor;

        //setting sizr
        this.sizeX = screenX/5;
        this.sizeY = screenX/5;
    }


    public Rocket(int screenX, int screenY, int gameType)
    {

        //initializing screen size
        this.screenX = screenX;
        this.screenY = screenY;

        //initializing player's body
        shape = new RectF();


        //initializing player's size
        if(gameType == 1)  //pong game
        {
            this.sizeX = screenX/4;
            this.sizeY = screenY/40;
        }
        if(gameType == 2)  //air hockey game
        {
            this.sizeX = screenX/5;
            this.sizeY = screenX/5;
        }
    }


    //updates rocket position to the center of the touched area
    public void update(Point point)
    {

            //limiting x position
            if(0 + 0.55*sizeX < point.x && point.x < screenX - 0.55*sizeX)
            {
                shape.left = (int) (point.x - sizeX/2);
                shape.right = (int)(shape.left + sizeX);
            }
            if(0 + 0.55*sizeX > point.x)
            {
                shape.left = (int) (0.15*sizeX);
                shape.right = (int)(shape.left + sizeX);
            }
            if(point.x > screenX - 0.55*sizeX)
            {
                shape.left = (int) (screenX - 1.15*sizeX);
                shape.right = (int)(shape.left + sizeX);
            }
            //limiting y position
            if(point.y > sizeY/2)
            {
                if(0 + 0.55*sizeY < point.y && point.y < screenY - 0.55*sizeY)
                {
                    shape.top = (int) (point.y + sizeY/2);
                    shape.bottom = (int)(shape.top - sizeY);
                }
                if(point.y > screenY - 0.55*sizeY)
                {
                    shape.top = (int) (screenY - 0.15*sizeY);
                    shape.bottom = (int)(shape.top - sizeY);
                }
            }
            if(point.y < screenY/2)
            {
                if(0 + 0.55*sizeY < point.y && point.y < screenY - 0.55*sizeY)
                {
                    shape.top = (int) (point.y + sizeY/2);
                    shape.bottom = (int)(shape.top - sizeY);
                }
                if(shape.top < 0.15*sizeY)
                {
                    shape.top = (int) (0.15*sizeY);
                    shape.bottom = (int)(shape.top - sizeY);
                }
            }

    }

    public void draw(Canvas canvas)
    {
        //initializing ball colorss+
        Paint paint1 = new Paint();
        paint1.setColor(borderColor);
        Paint paint2 = new Paint();
        paint2.setColor(color);

        //drawing edges
        RectF tempRect = new RectF(shape.left + sizeX/10, shape.top - sizeY/10, shape.right - sizeX/10, shape.bottom + sizeY/10);

        //drawing rects as ovals
        canvas.drawOval(shape, paint1);
        canvas.drawOval(tempRect, paint2);
    }

}
